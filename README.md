# README #

Gulp autoprefixer plagin for CSS;

# -- code --

```
#!javascript

var gulp = require('gulp');
var autoprefixer = require('autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var postcss = require('gulp-postcss');

gulp.task('autoprefix', function(){
	gulp.src('style/style.css')
		.pipe(sourcemaps.init())
        .pipe(postcss([ autoprefixer({ browsers: ['last 2 versions'] }) ]))
        .pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('./css')) 
});

gulp.task('default',function () {
    gulp.run('autoprefix');
});
```
# -- end of code --